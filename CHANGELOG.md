Release 0.0.1:
* Shell commands storage

Release 0.0.2:
* Required APT packages

Release 0.0.3:
* History of runned commands, with history and logs commands.

Release 0.0.4:
* Switch to SQLite database.
