#!/bin/bash

set -x

test/integration_test_code.sh > test/integration_test_actual.txt 2>&1

diff test/integration_test_actual.txt test/integration_test_expected.txt
