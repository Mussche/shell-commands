#!/bin/bash

HOME=$(mktemp -d -t cdb-it-XXXXXXXXXXXXXXXXXXX)

python3.7 -m venv $HOME/venv
# shellcheck disable=SC1090
source ${HOME}/venv/bin/activate
pip install --upgrade pip wheel > /dev/null 2>&1
pip install /work/cdb > /dev/null 2>&1
#pip install shell_commands==0.0.2 > /dev/null 2>&1

set -e
set -x

commands --help
commands list
commands save --cwd /home --user klaas test ls /usr
commands list
commands show test
commands delete test
commands list
commands run test
commands history --simple
commands save --cwd /work/cdb cdb-test-chat ./test/chat.py
commands run cdb-test-chat

commands apt --help
commands apt require some-package
commands apt require some-other-package
commands apt list
commands apt unrequire some-package
commands apt list

set +x
rm -rf $HOME
unset HOME

